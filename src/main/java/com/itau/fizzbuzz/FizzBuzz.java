package com.itau.fizzbuzz;

import java.util.ArrayList;
import java.util.List;

public class FizzBuzz {

	public static String fizzBuzz(int i) {
//		if (i % 3 == 0) {
//			return "fizz";
//		}
//		return null;
		if ((i % 3 == 0) && (i % 5 == 0)) {
			return "fizzbuzz";
		}

		if (i % 5 == 0) {
			return "buzz";
		}

		if (i % 3 == 0) {
			return "fizz";

		}

		return Integer.toString(i);

	}

	public static List<String> fizzBuzzFull(int valor) {
		
		List<String> lista = new ArrayList<String>();

		for (int i = 1; i <= valor; i++) {

			if ((i % 3 == 0) && (i % 5 == 0)) {
				lista.add("fizzbuzz");
			} else if (i % 5 == 0) {
				lista.add("buzz");
			} else if (i % 3 == 0) {
				lista.add("fizz");
			} else {
				lista.add(String.valueOf(i));
			}
		}

		return lista;
	}

}
