package com.itau.fizzbuzz;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {

	@Test
	public void testaFizzBuzz3() {
		String ret = FizzBuzz.fizzBuzz(3);
		assertEquals(ret, "fizz");
	}

	@Test
	public void testaFizzBuzz5() {
		String ret = FizzBuzz.fizzBuzz(5);
		assertEquals(ret, "buzz");
	}

	@Test
	public void testaFizzBuzz() {
		String ret = FizzBuzz.fizzBuzz(3);
		assertEquals(ret, "fizz");
	}

	@Test
	public void testaFizzBuzz15() {
		String ret = FizzBuzz.fizzBuzz(15);
		assertEquals(ret, "fizzbuzz");
	}

	@Test
	public void testaFizzBuzzmulti3() {
		String ret = FizzBuzz.fizzBuzz(6);
		assertEquals(ret, "fizz");
	}

	@Test
	public void testaFizzBuzzmulti5() {
		String ret = FizzBuzz.fizzBuzz(10);
		assertEquals(ret, "buzz");
	}

	@Test
	public void testaFizzBuzzmulti15() {
		String ret = FizzBuzz.fizzBuzz(30);
		assertEquals(ret, "fizzbuzz");
	}

	@Test
	public void testaFizzBuzzNada() {
		String ret = FizzBuzz.fizzBuzz(17);
		assertEquals(ret, "17");
	}
	
	@Test
	public void testeLista()
	{
		List<String> gabarito = new ArrayList<String>();
		
		gabarito.add("1");
		gabarito.add("2");
		gabarito.add("fizz");
		gabarito.add("4");
		gabarito.add("buzz");
		gabarito.add("fizz");
		gabarito.add("7");
		gabarito.add("8");
		gabarito.add("fizz");
		gabarito.add("buzz");
		gabarito.add("11");
		gabarito.add("fizz");
		gabarito.add("13");
		gabarito.add("14");
		gabarito.add("fizzbuzz");
		
		List<String> ret = FizzBuzz.fizzBuzzFull(15);
		
		assertThat(gabarito, is(ret));
				
	}

}
